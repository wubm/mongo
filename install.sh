#!/bin/bash
DIR_PATH=$(dirname $(readlink -f $0))

change_config() {
    local conf="$DIR_PATH/docker-compose.yml"
    sed -i "s/<replica_name>/$REPLICA_NAME/g" $conf
    sed -i "s/<mongo1_name>/$MONGO1_NAME/g" $conf
    sed -i "s/<mongo1_ip>/$MONGO1_IP/g" $conf
    sed -i "s/<mongo2_name>/$MONGO2_NAME/g" $conf
    sed -i "s/<mongo2_ip>/$MONGO2_IP/g" $conf
    sed -i "s/<mongo3_name>/$MONGO3_NAME/g" $conf
    sed -i "s/<mongo3_ip>/$MONGO3_IP/g" $conf
    sed -i "s/<ops_manager01_name>/$OPS_MANAGER01_NAME/g" $conf
    sed -i "s/<ops_manager01_ip>/$OPS_IP/g" $conf
    sed -i "s/<ops_domain>/$OPS_DOMAIN/g" $conf
    sed -i "s/<mongo_node_hostname>/container_$(hostname)/g" $conf
    sed -i "s/<your_groupid>/$AGENT_MMSGROUPID/g" $conf
    sed -i "s/<your_mmsapikey>/$AGENT_MMSAPIKEY/g" $conf
    sed -i "s#<your_mmsbaseurl>#$OPS_DOMAIN_URL#g" $conf
    sed -i "s/<your_serverpoolkey>/$AGENT_SERVERPOOLKEY/g" $conf
    sed -i "s/<your_datacenter>/$AGENT_DATACENTER/g" $conf
    
    local conf="$DIR_PATH/mongo/replica.js"
    sed -i "s/<replica_name>/$REPLICA_NAME/g" $conf
    sed -i "s/<mongo1_name>/$MONGO1_NAME/g" $conf
    sed -i "s/<mongo2_name>/$MONGO2_NAME/g" $conf
    sed -i "s/<mongo3_name>/$MONGO3_NAME/g" $conf

}

main() {
    if [ ! -f $DIR_PATH/configure.conf ] ; then
        #cp $DIR_PATH/configure.conf.default $DIR_PATH/configure.conf
        echo "configure.conf not exist! Please copy configure.conf.default to configure.conf and modify it"
        exit 1
    fi
    
    if [ ! -f $DIR_PATH/docker-compose.yml ] ; then
        cp $DIR_PATH/docker-compose.yml.default $DIR_PATH/docker-compose.yml
    fi

    if [ ! -f $DIR_PATH/mongo/replica.js ] ; then
        cp $DIR_PATH/mongo/replica.js.default $DIR_PATH/mongo/replica.js
    fi

    source $DIR_PATH/configure.conf
    change_config

}

main "$@"
