docker exec -ti container bash
mongo < /tmp/replica.js

admin = db.getSiblingDB('admin');
admin.createUser({
  user: 'user_name',
  pwd: 'password',
  roles: [{ role: 'userAdminAnyDatabase', db: 'admin' }]
});
db.getSiblingDB('admin').auth('user_name', 'password');
