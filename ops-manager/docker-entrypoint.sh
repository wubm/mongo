#!/bin/bash
DIR_PATH=$(dirname $(readlink -f $0))

change_config() {
    local conf="/opt/mongodb/mms/conf/conf-mms.properties"
    sed -i "s#^mongo.mongoUri=.*#mongo.mongoUri=mongodb://$MONGO1_NAME:$MONGO1_PORT,$MONGO2_NAME:$MONGO2_PORT,$MONGO3_NAME:$MONGO3_PORT#g" $conf
    #if [ -f "/etc/ssl/temphawk.net.crt" ] && [ -f "/etc/ssl/temphawk.net.key" ] ; then
    #    #sed -i "s#^mongo.ssl=false#mongo.ssl=true#g" $conf
    #    sed -i "s#^mongodb.ssl.CAFile=.*#mongodb.ssl.CAFile=/etc/ssl/temphawk.net.crt#g" $conf
    #    sed -i "s#^mongodb.ssl.PEMKeyFile=.*#mongodb.ssl.PEMKeyFile=/etc/ssl/temphawk.net.key#g" $conf
    #fi
    #sed -i "s/^jvm.java.security.krb5/# jvm.java.security.krb5/g" $conf
    #sed -i "s/^mms.kerberos/# mms.kerberos/g" $conf
}

main() {
    change_config

    exec /usr/bin/supervisord
    #exec /sbin/init
}

main "$@"
