#!/bin/bash
DIR_PATH=$(dirname $(readlink -f $0))

change_config() {
    local conf="/etc/mongodb-mms/automation-agent.config"
    sed -i "s#^mmsGroupId=.*#mmsGroupId=$MMSGROUPID#g" $conf
    sed -i "s#^mmsApiKey=.*#mmsApiKey=$MMSAPIKEY#g" $conf
    sed -i "s#^mmsBaseUrl=.*#mmsBaseUrl=$MMSBASEURL#g" $conf
    sed -i "s#^logFile=.*#logFile=$LOGFILE#g" $conf
    sed -i "s#^mmsConfigBackup=.*#mmsConfigBackup=$MMSCONFIGBACKUP#g" $conf
    sed -i "s#^logLevel=.*#logLevel=$LOGLEVEL#g" $conf
    sed -i "s#^maxLogFiles=.*#maxLogFiles=$MAXLOGFILES#g" $conf
    sed -i "s#^maxLogFileSize=.*#maxLogFileSize=$MAXLOGFILESIZE#g" $conf
    sed -i "s#^serverPoolKey=.*#serverPoolKey=$SERVERPOOLKEY#g" $conf
    
    echo "Datacenter=$DATACENTER" > /etc/mongodb-mms/server-pool.properties
}

main() {
    change_config

    exec /usr/bin/supervisord
    #exec /sbin/init
}

main "$@"
